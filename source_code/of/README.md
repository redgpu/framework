```
$ uname -r
4.4.0-21-generic
```
```
$ c++ --version
c++ (Ubuntu 5.4.0-6ubuntu1~16.04.12) 5.4.0 20160609
```
```
sudo apt-get -y install   \
gcc                       \
g++                       \
git                       \
gdb                       \
pkg-config                \
xorg-dev                  \
libgtk-3-dev              \
libboost-filesystem-dev   \
libboost-system-dev       \
libudev-dev               \
cmake                     \
ninja-build               \
libcairo2-dev             \
libusb-1.0-0-dev          \
libssl-dev                \
libcurl4-openssl-dev      \
libfreetype6-dev          \
libfontconfig1-dev        \
libglu1-mesa-dev          \
libmpg123-dev             \
libopenal-dev             \
libopencv-dev             \
libtbb-dev                \
libasound2-dev            \
libsndfile1-dev           \
gstreamer1.0-x            \
gstreamer1.0-alsa         \
gstreamer1.0-libav        \
gstreamer1.0-pulseaudio   \
gstreamer1.0-plugins-base \
gstreamer1.0-plugins-good \
gstreamer1.0-plugins-bad  \
gstreamer1.0-plugins-ugly \
libgstreamer1.0-dev       \
libgstreamer-plugins-base1.0-dev
```
https://github.com/openframeworks/openFrameworks/tree/0e736756552de614fe71e58708657f0a7a223bbe
