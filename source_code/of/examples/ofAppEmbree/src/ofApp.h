#pragma once

#include "ofMain.h"
#include "Embree.h"

class ofApp : public ofBaseApp {
public:
  void setup         ();
  void exit          ();
  void update        ();
  void draw          ();
  void keyPressed    (int key);
  void windowResized (int w, int h);
  void mouseDragged  (int x, int y, int button);
  void mouseReleased (int x, int y, int button);
};
