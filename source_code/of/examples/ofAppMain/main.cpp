#include "ofMain.h"

class ofApp : public ofBaseApp {
public:
  void setup  ();
  void update ();
  void draw   ();
  void exit   ();

  void keyPressed      (ofKeyEventArgs &);
  void keyReleased     (ofKeyEventArgs &);

  void mouseMoved      (ofMouseEventArgs &);
  void mouseDragged    (ofMouseEventArgs &);
  void mousePressed    (ofMouseEventArgs &);
  void mouseReleased   (ofMouseEventArgs &);
  void mouseScrolled   (ofMouseEventArgs &);
  void mouseEntered    (ofMouseEventArgs &);
  void mouseExited     (ofMouseEventArgs &);

  void touchDown       (ofTouchEventArgs &);
  void touchMoved      (ofTouchEventArgs &);
  void touchUp         (ofTouchEventArgs &);
  void touchDoubleTap  (ofTouchEventArgs &);
  void touchCancelled  (ofTouchEventArgs &);

  void windowResized   (ofResizeEventArgs &);
  void dragged         (ofDragInfo &);
  void messageReceived (ofMessage &);
};

void ofApp::setup() {
}

void ofApp::update() {
}

void ofApp::draw() {
}

void ofApp::exit() {
}

void ofApp::keyPressed(ofKeyEventArgs & key) {
}

void ofApp::keyReleased(ofKeyEventArgs & key) {
}

void ofApp::mouseMoved(ofMouseEventArgs & mouse) {
}

void ofApp::mouseDragged(ofMouseEventArgs & mouse) {
}

void ofApp::mousePressed(ofMouseEventArgs & mouse) {
}

void ofApp::mouseReleased(ofMouseEventArgs & mouse) {
}

void ofApp::mouseScrolled(ofMouseEventArgs & mouse) {
}

void ofApp::mouseEntered(ofMouseEventArgs & mouse) {
}

void ofApp::mouseExited(ofMouseEventArgs & mouse) {
}

void ofApp::touchDown(ofTouchEventArgs & touch) {
}

void ofApp::touchMoved(ofTouchEventArgs & touch) {
}

void ofApp::touchUp(ofTouchEventArgs & touch) {
}

void ofApp::touchDoubleTap(ofTouchEventArgs & touch) {
}

void ofApp::touchCancelled(ofTouchEventArgs & touch) {
}

void ofApp::windowResized(ofResizeEventArgs & window) {
}

void ofApp::dragged(ofDragInfo & dragged) {
}

void ofApp::messageReceived(ofMessage & message) {
}

int main() {
  ofGLFWWindowSettings settings;

  std::shared_ptr<ofAppBaseWindow> window = ofCreateWindow(settings);
  std::shared_ptr<ofApp> app = make_shared<ofApp>();
  ofRunApp(window, app);

  return ofRunMainLoop();
}