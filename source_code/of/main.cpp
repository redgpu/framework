#if 0
c++ -std=c++11 -DTARGET_NO_VIDEO main.cpp -pthread libopenFrameworks.a libglfw.a -lboost_system -lboost_filesystem -lcurl -lz -lcairo -lfontconfig -lfreetype -lX11 -lGL -lopenal -lmpg123 -lsndfile -lXxf86vm -lXrandr -lXcursor -lXi -lXinerama
exit
#endif

#include "ofMain.h"

class ofApp : public ofBaseApp {
public:
  void setup  ();
  void update ();
  void draw   ();
  void exit   ();

  void keyPressed      (ofKeyEventArgs &);
  void keyReleased     (ofKeyEventArgs &);

  void mouseMoved      (ofMouseEventArgs &);
  void mouseDragged    (ofMouseEventArgs &);
  void mousePressed    (ofMouseEventArgs &);
  void mouseReleased   (ofMouseEventArgs &);
  void mouseScrolled   (ofMouseEventArgs &);
  void mouseEntered    (ofMouseEventArgs &);
  void mouseExited     (ofMouseEventArgs &);

  void touchDown       (ofTouchEventArgs &);
  void touchMoved      (ofTouchEventArgs &);
  void touchUp         (ofTouchEventArgs &);
  void touchDoubleTap  (ofTouchEventArgs &);
  void touchCancelled  (ofTouchEventArgs &);

  void windowResized   (ofResizeEventArgs &);
  void dragged         (ofDragInfo &);
  void messageReceived (ofMessage &);
  
  ofEasyCam cam;
};

void ofApp::setup() {
  //cam.removeAllInteractions();
  //cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY, OF_MOUSE_BUTTON_LEFT);
  //cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_Z, OF_MOUSE_BUTTON_RIGHT);
  //cam.enableOrtho();
  //cam.setNearClip(-1000000);
  //cam.setFarClip(1000000);
  //cam.setVFlip(true);
}

void ofApp::update() {
}

void ofApp::draw() {
  cam.begin();
  ofNoFill();
  ofDrawSphere(0.0, 0.0, 0, 200);
  ofDrawSphere(-500, 400, 0, 200);
  ofDrawSphere(500, 400, 0, 200);
  ofDrawSphere(-500, -400, 0, 200);
  ofDrawSphere(500, -400, 0, 200);
  cam.end();
}

void ofApp::exit() {
}

void ofApp::keyPressed(ofKeyEventArgs & key) {
}

void ofApp::keyReleased(ofKeyEventArgs & key) {
}

void ofApp::mouseMoved(ofMouseEventArgs & mouse) {
}

void ofApp::mouseDragged(ofMouseEventArgs & mouse) {
}

void ofApp::mousePressed(ofMouseEventArgs & mouse) {
}

void ofApp::mouseReleased(ofMouseEventArgs & mouse) {
}

void ofApp::mouseScrolled(ofMouseEventArgs & mouse) {
}

void ofApp::mouseEntered(ofMouseEventArgs & mouse) {
}

void ofApp::mouseExited(ofMouseEventArgs & mouse) {
}

void ofApp::touchDown(ofTouchEventArgs & touch) {
}

void ofApp::touchMoved(ofTouchEventArgs & touch) {
}

void ofApp::touchUp(ofTouchEventArgs & touch) {
}

void ofApp::touchDoubleTap(ofTouchEventArgs & touch) {
}

void ofApp::touchCancelled(ofTouchEventArgs & touch) {
}

void ofApp::windowResized(ofResizeEventArgs & window) {
}

void ofApp::dragged(ofDragInfo & dragged) {
}

void ofApp::messageReceived(ofMessage & message) {
}

int main() {
  ofGLFWWindowSettings settings;

  auto window = ofCreateWindow(settings);
  auto app = make_shared<ofApp>();
  ofRunApp(window, app);

  return ofRunMainLoop();
}