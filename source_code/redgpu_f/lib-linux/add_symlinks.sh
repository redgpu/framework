mkdir -p debug-x86-64
cd debug-x86-64
rm -f libassimp.so.4
rm -f libboost_filesystem.so.1.71.0
ln -s ../../ofxaddons/assimp/libassimp.so.4 .
ln -s ../../ubuntu16046/boost_filesystem/lib/libboost_filesystem.so.1.71.0 .
cd ..

mkdir -p release-x86-64
cd release-x86-64
rm -f libassimp.so.4
rm -f libboost_filesystem.so.1.71.0
ln -s ../../ofxaddons/assimp/libassimp.so.4 .
ln -s ../../ubuntu16046/boost_filesystem/lib/libboost_filesystem.so.1.71.0 .
cd ..
