#pragma once

#include "ofMain.h"
#include "../../ofxCubeMap/src/ofxCubeMap.h"

namespace ofxEquiMap
{
    class Renderer
    {
    protected:
        ofxCubeMap cm;
        ofShader warpShader;
    public:
        void setup(int size, int internalformat = GL_RGB);
        virtual void render(void (*drawEquiScene)(void *), void * userData, float cubePosX, float cubePosY, float cubePosZ);
        void draw(float x, float y, float w, float h);
    };
    
    class CustomFboRenderer : public Renderer
    {
    protected:
        ofFbo fbo;
    public:
        void setup(int size, int internalformat = GL_RGB, int numSamples = 0);
        void setup(int size, ofFbo::Settings fbo_settings);
        void render(void (*drawEquiScene)(void *), void * userData, float cubePosX, float cubePosY, float cubePosZ) override;
        
        ofFbo& getFbo() { return fbo; }
        const ofFbo& getFbo() const { return fbo; }
    };
};