ofxGifRecorder
==============


This addon records everything from ofApp window and creates a gif file from it. 
Make an instance of `ofxGifRecorder` class called `gif` and toggle the recording with a single `gif.toggleRecording()` method.


Dependencies
------------

#### 1. OF 0.9.0 and C++14
#### 2. [ofxGifEncoder](https://github.com/ofnode/ofxGifEncoder)


Compiling
---------

For [openFrameworks](https://github.com/openframeworks/openFrameworks):

[See wiki](https://github.com/ofnode/of/wiki/Compiling-ofApp-with-vanilla-openFrameworks)

For [CMake-based openFrameworks](https://github.com/ofnode/of):

Add this repo as a git submodule to your [ofApp](https://github.com/ofnode/ofApp) folder and use `ofxaddon` command in `CMakeLists.txt`.

