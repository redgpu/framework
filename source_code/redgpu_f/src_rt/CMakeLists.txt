project(redgpu_f_rt)
cmake_minimum_required(VERSION 2.8.12)

set(CMAKE_CXX_STANDARD 14)

set(CMAKE_C_VISIBILITY_PRESET hidden)
set(CMAKE_CXX_VISIBILITY_PRESET hidden)
set(CMAKE_VISIBILITY_INLINES_HIDDEN 1)

set(OF_ROOT_DIR "${CMAKE_CURRENT_LIST_DIR}/..")

set(OPENFRAMEWORKS_INCLUDE_DIRS
    "${OF_ROOT_DIR}"

    "${OF_ROOT_DIR}/src/openframeworks"
    "${OF_ROOT_DIR}/src/openframeworks/3d"
    "${OF_ROOT_DIR}/src/openframeworks/app"
    "${OF_ROOT_DIR}/src/openframeworks/communication"
    "${OF_ROOT_DIR}/src/openframeworks/events"
    "${OF_ROOT_DIR}/src/openframeworks/gl"
    "${OF_ROOT_DIR}/src/openframeworks/graphics"
    "${OF_ROOT_DIR}/src/openframeworks/math"
    "${OF_ROOT_DIR}/src/openframeworks/sound"
    "${OF_ROOT_DIR}/src/openframeworks/types"
    "${OF_ROOT_DIR}/src/openframeworks/utils"
    "${OF_ROOT_DIR}/src/openframeworks/video"

    "${OF_ROOT_DIR}/src/freeimage/Source"
    "${OF_ROOT_DIR}/src/freeimage/Source/DeprecationManager"
    "${OF_ROOT_DIR}/src/freeimage/Source/LibJPEG"
    "${OF_ROOT_DIR}/src/freeimage/Source/LibOpenJPEG"
    "${OF_ROOT_DIR}/src/freeimage/Source/LibPNG"
    "${OF_ROOT_DIR}/src/freeimage/Source/LibRawLite"
    "${OF_ROOT_DIR}/src/freeimage/Source/LibTIFF4"
    "${OF_ROOT_DIR}/src/freeimage/Source/LibWebP"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Half"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Iex"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IexMath"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmThread"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath"

    "${OF_ROOT_DIR}/src/glew"
    "${OF_ROOT_DIR}/src/glew/include"

    "${OF_ROOT_DIR}/src/glfw"
    "${OF_ROOT_DIR}/src/glfw/include"
    "${OF_ROOT_DIR}/src/glfw/include/GLFW"

    "${OF_ROOT_DIR}/src/glm/include"

    "${OF_ROOT_DIR}/src/kissfft"
    "${OF_ROOT_DIR}/src/kissfft/tools"

    "${OF_ROOT_DIR}/src/libtess2"
    "${OF_ROOT_DIR}/src/libtess2/Include"
    "${OF_ROOT_DIR}/src/libtess2/Source"

    #"${OF_ROOT_DIR}/src/poco"
    #"${OF_ROOT_DIR}/src/poco/Crypto/include"
    #"${OF_ROOT_DIR}/src/poco/Foundation/include"
    #"${OF_ROOT_DIR}/src/poco/Net/include"
    #"${OF_ROOT_DIR}/src/poco/NetSSL_OpenSSL/include"
    #"${OF_ROOT_DIR}/src/poco/Util/include"
    #"${OF_ROOT_DIR}/src/poco/XML/include"
    #"${OF_ROOT_DIR}/src/poco/Zip/include"

    "${OF_ROOT_DIR}/src/rtaudio"
    "${OF_ROOT_DIR}/src/rtaudio/include"

    "${OF_ROOT_DIR}/src/utf8cpp"
    "${OF_ROOT_DIR}/src/utf8cpp/include"

    "${OF_ROOT_DIR}/src/uriparser/include"

    "${OF_ROOT_DIR}/src/pugixml/src"

    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/include"
    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/include/renderer"
)

set(OPENFRAMEWORKS_SOURCES
    "${OF_ROOT_DIR}/src_rt/redgpu_f_rt.cpp"
)

set(OPENEXR_SOURCES
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/b44ExpLogTable.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfAcesFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfB44Compressor.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfBoxAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfChannelList.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfChannelListAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfChromaticities.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfChromaticitiesAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfCompositeDeepScanLine.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfCompressionAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfCompressor.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfConvert.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfCRgbaFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepCompositing.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepFrameBuffer.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepImageStateAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepScanLineInputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepScanLineInputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepScanLineOutputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepScanLineOutputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepTiledInputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepTiledInputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepTiledOutputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDeepTiledOutputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDoubleAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfDwaCompressor.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfEnvmap.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfEnvmapAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfFastHuf.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfFloatAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfFloatVectorAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfFrameBuffer.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfFramesPerSecond.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfGenericInputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfGenericOutputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfHeader.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfHuf.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfInputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfInputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfInputPartData.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfIntAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfIO.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfKeyCode.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfKeyCodeAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfLineOrderAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfLut.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfMatrixAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfMisc.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfMultiPartInputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfMultiPartOutputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfMultiView.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfOpaqueAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfOutputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfOutputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfOutputPartData.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfPartType.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfPizCompressor.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfPreviewImage.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfPreviewImageAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfPxr24Compressor.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfRational.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfRationalAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfRgbaFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfRgbaYca.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfRle.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfRleCompressor.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfScanLineInputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfStandardAttributes.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfStdIO.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfStringAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfStringVectorAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfSystemSpecific.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTestFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfThreading.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTileDescriptionAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTiledInputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTiledInputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTiledMisc.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTiledOutputFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTiledOutputPart.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTiledRgbaFile.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTileOffsets.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTimeCode.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfTimeCodeAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfVecAttribute.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfVersion.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfWav.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfZip.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmImf/ImfZipCompressor.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath/ImathBox.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath/ImathColorAlgo.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath/ImathFun.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath/ImathMatrixAlgo.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath/ImathRandom.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath/ImathShear.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Imath/ImathVec.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Iex/IexBaseExc.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Iex/IexThrowErrnoExc.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/Half/half.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmThread/IlmThread.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmThread/IlmThreadMutex.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmThread/IlmThreadPool.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IlmThread/IlmThreadSemaphore.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IexMath/IexMathFloatExc.cpp"
    "${OF_ROOT_DIR}/src/freeimage/Source/OpenEXR/IexMath/IexMathFpu.cpp"
)

find_package(PkgConfig REQUIRED)
pkg_check_modules(CAIRO REQUIRED cairo)

list(APPEND OPENFRAMEWORKS_INCLUDE_DIRS
    ${CAIRO_INCLUDE_DIRS}
)

add_definitions(-Wno-deprecated -DTARGET_NO_VIDEO)
include_directories(${OPENFRAMEWORKS_INCLUDE_DIRS})

add_library(
    redgpu_f_rt SHARED
    ${OPENFRAMEWORKS_SOURCES}
    ${OPENEXR_SOURCES}
)

if(CMAKE_SYSTEM MATCHES Linux)
  list(APPEND OPENFRAMEWORKS_LIBRARIES
    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/lib/ubuntu1804-x64/libloaders.a"
    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/lib/ubuntu1804-x64/librenderer.a"
    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/lib/ubuntu1804-x64/librtcore.a"
    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/lib/ubuntu1804-x64/libsys.a"
    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/lib/ubuntu1804-x64/libimage.a"
    "${OF_ROOT_DIR}/ofxaddons/ofxEmbree/libs/embree/lib/ubuntu1804-x64/liblexers.a"
  )
endif()

target_link_libraries(
    redgpu_f_rt
    ${OPENFRAMEWORKS_LIBRARIES}
)
